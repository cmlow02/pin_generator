#include "Combinations.h"
#include <iomanip>
#include <algorithm>
#include <iostream>
#include "Utils.h"
#include <random>

std::array<std::string, 10> disallowed{"0000","1111","2222","3333","4444","5555","6666","7777","8888","9999"};

Combinations::Combinations() 
{
    int iter=0;

    auto N = ncombinations - 10;

    combi = std::vector<std::string>(N, "");
    combiIdx = std::vector<int>(N, -1);

    std::stringstream ss;
    for (int i=0; i<ncombinations;i++){
        ss << std::setfill('0') << std::setw(4) << i;
        auto p = std::find(disallowed.begin(), disallowed.end(), ss.str());
        if (p == disallowed.end()){
            combi[iter] = ss.str();
            combiIdx[iter] = iter;
            iter++;
        }
        ss.str("");
        ss.clear();
    }
}


Combinations* Combinations::getInstance() 
{
    if (!instance)
        instance = new Combinations;
    return instance;
}


int Combinations::getNextCombiIdx(std::vector<int>& existingIdx) const
{
    auto allidx = combiIdx;

    std::vector<int>::iterator it;

    for (auto i : existingIdx){
        it = std::remove_if(allidx.begin(), allidx.end(), [&i](int idx){ return (idx == i); });
        allidx.erase(it, allidx.end());
    }

    if (!allidx.empty()){
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist6(0, allidx.size()); // distribution in range [1, 6]

        return allidx[dist6(rng)];
    }
    return -1;
}


Combinations *Combinations::instance = nullptr;


Combinations::~Combinations() 
{
    delete instance;
}



std::string Combinations::getPin(int pinidx) const
{
    return combi.at(pinidx);
}


size_t Combinations::getcombiIdxSize() const
{
    return combiIdx.size();
}
