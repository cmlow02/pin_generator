#ifndef __COMBINATIONS_H__
#define __COMBINATIONS_H__

#include "Utils.h"
#include <vector>

class CombinationsTest;
class UserFriend;

class Combinations
{
    friend class CombinationsTest;
    friend class UserFriend;

    static Combinations *instance;
    std::vector<std::string> combi;
    std::vector<int> combiIdx;

    Combinations();
    ~Combinations();
public:
    static Combinations *getInstance();

    int getNextCombiIdx(std::vector<int>& existingIdx) const;

    std::string getPin(int pinidx) const;

    size_t getcombiIdxSize() const;

};




#endif // __COMBINATIONS_H__