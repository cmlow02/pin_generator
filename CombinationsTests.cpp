#include <gtest/gtest.h>
#include "Combinations.h"
#include "Utils.h"

TEST(Combinations, getNextCombiIdx){
    auto pCombi = Combinations::getInstance();

    std::vector<int> existingIdx{4, 8, 10};

    int nextidx = -1;

    nextidx = pCombi->getNextCombiIdx(existingIdx);

    EXPECT_GE(nextidx, -1);
}

struct CombinationsTest : public ::testing::Test
{
    std::vector<int> getcombiIdx(Combinations* p){
        return p->combiIdx;
    }
};

TEST_F(CombinationsTest, getNextCombiIdxEmpty){
    auto pCombi = Combinations::getInstance();

    std::vector<int> existingIdx = getcombiIdx(pCombi);

    int nextidx = -1;

    nextidx = pCombi->getNextCombiIdx(existingIdx);
    
    EXPECT_EQ(nextidx, -1);
}