#include "Encoder.h"
#include <openssl/evp.h>
#include <openssl/conf.h>
#include <cstring>
#include "Utils.h"

IEncoder::~IEncoder() 
{
}


int OpenSSLAES::encrypt(const std::string& key, const std::string& in, unsigned char* encrypted, const unsigned int maxlen) 
{
    unsigned char* key_char = reinterpret_cast<unsigned char*>(const_cast<char*>(key.c_str()));
    unsigned char* text_char = reinterpret_cast<unsigned char*>(const_cast<char*>(in.c_str()));
    
    int text_len = strlen((const char*)text_char);

    auto cipher_len = encrypt_ssl(text_char, text_len, key_char, encrypted, maxlen);

    return cipher_len;
}

int OpenSSLAES::decrypt(unsigned char* in, int cipher_len, const std::string& key, std::string& decrypted, const unsigned int maxlen)
{
    unsigned char* key_char = reinterpret_cast<unsigned char*>(const_cast<char*>(key.c_str()));

    unsigned char decrypted_char[maxlen];

    int dec_len = decrypt_ssl(in, cipher_len, key_char, decrypted_char, maxlen);

    unsigned_char_to_string(decrypted_char, dec_len, decrypted);

    return dec_len;
}


int OpenSSLAES::encrypt_ssl(unsigned char* text, int text_len, unsigned char* key, unsigned char* cipher, unsigned int max_cipher_size)
{
    unsigned int cipher_len = 0;
    int len = 0;

    EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

    if (!ctx)
        return false;
    
    auto proceed = EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);

    if (!proceed)
        return false;

    proceed = EVP_EncryptUpdate(ctx, cipher, &len, text, text_len);
        
    if (!proceed)
        return false;

    cipher_len += len;

    proceed = EVP_EncryptFinal_ex(ctx, cipher + len, &len);

    cipher_len += len;

    EVP_CIPHER_CTX_free(ctx);

    if (cipher_len > max_cipher_size)
        throw(std::length_error("Required cipher length exceeds allocated length."));

    return cipher_len;
}


int OpenSSLAES::decrypt_ssl(unsigned char* cipher, int cipher_len, unsigned char* key, unsigned char* text, unsigned int max_text_size)
{
    unsigned int text_len = 0;
    int len = 0;
    
    EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

    if (!ctx)
        return false;
    
    auto proceed = EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);

    if (!proceed)
        return false;

    proceed = EVP_DecryptUpdate(ctx, text, &len, cipher, cipher_len);

    if (!proceed)
        return false;

    text_len += len;

    proceed = EVP_DecryptFinal_ex(ctx, text + len, &len);

    if (!proceed)
        return false;

    text_len += len;

    EVP_CIPHER_CTX_free(ctx);

    if (text_len > max_text_size)
        throw(std::length_error("Required text length exceeds allocated length."));

    return text_len;
}


OpenSSLAES::OpenSSLAES() 
{

}

OpenSSLAES::~OpenSSLAES() 
{

}