#ifndef __CIPHER_H__
#define __CIPHER_H__

#include <string>
#include <stdexcept>

class IEncoder
{
    public:
        virtual int encrypt(const std::string& key, const std::string& in, unsigned char* encrypted, const unsigned int maxlen) = 0;
        virtual int decrypt(unsigned char* in, int cipher_len, const std::string& key, std::string& decrypted, const unsigned int maxlen) = 0;
        virtual ~IEncoder();
};


class OpenSSLAES : public IEncoder
{
    public:
        OpenSSLAES();
        virtual ~OpenSSLAES();

        int encrypt(const std::string& key, const std::string& in, unsigned char* encrypted, const unsigned int maxlen) override;
        int decrypt(unsigned char* in, int cipher_len, const std::string& key, std::string& decrypted, const unsigned int maxlen) override;

    public:
        int encrypt_ssl(unsigned char* text, int text_len, unsigned char* key, unsigned char* cipher, unsigned int max_cipher_size);
        int decrypt_ssl(unsigned char* cipher, int cipher_len, unsigned char* key, unsigned char* text, unsigned int max_text_size);
};


#endif // __CIPHER_H__