#include "UserArchiver.h"
#include "User.h"
#include <gtest/gtest.h>
#include <memory>
#include <cstdio>

struct EncoderTestFix : public ::testing::Test
{
    IEncoder* encoder;

    void SetUp(){
        encoder = new OpenSSLAES;
    }
    void TearDown(){
        delete encoder;
    }
};

TEST_F(EncoderTestFix, MessageEncryptionDecryption){

    std::string key{"0123456789abcdef"};
    std::string text{"My secret message!"};

    const int maxlen = 4096;
    unsigned char cipher[maxlen];

    auto cipher_len = encoder->encrypt(key, text, cipher, maxlen);

    std::string decrypted;

    encoder->decrypt(cipher, cipher_len, key, decrypted, maxlen);

    EXPECT_EQ(decrypted, text);

}

