#include "Hasher.h"
#include <sstream>
#include <iomanip>

OpenSSLHasher::OpenSSLHasher() 
{
};

std::string OpenSSLHasher::getHashKey(std::string s) 
{
    unsigned char hash[SHA256_DIGEST_LENGTH];

    SHA256_CTX* checksum = new SHA256_CTX;

    SHA256_Init(checksum);
    
    SHA256_Update(checksum, s.c_str(), s.size());
    
    SHA256_Final(hash, checksum);
    
    std::stringstream ss;
    for (int i=0; i<SHA256_DIGEST_LENGTH; i++){
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
        // ss << std::hex << (int)hash[i];
    }
    
    delete checksum;
    
    return ss.str();
}

OpenSSLHasher::~OpenSSLHasher() 
{
}
