#ifndef __HASHER_H__
#define __HASHER_H__

#include <string>
#include <openssl/sha.h>

class IHasher
{
    public:
        virtual std::string getHashKey(std::string s) = 0;
        virtual ~IHasher(){};
};

class OpenSSLHasher : public IHasher
{
    public:
        std::string getHashKey(std::string s) override;
        OpenSSLHasher();
        ~OpenSSLHasher();
        
};


#endif // __HASHER_H__