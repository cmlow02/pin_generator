ROOTDIR = $(shell pwd)

BUILD_DIR=build
BUILD_PATH=./$(BUILD_DIR)
BUILD_PATH_EXISTS=$(shell find . -name $(BUILD_DIR) -type d)

INSTALL_DIR=install
INSTALL_PATH=./$(INSTALL_DIR)
INSTALL_PATH_EXISTS=$(shell find . -name $(INSTALL_DIR) -type d)

CC=g++
CSTD=-std=c++17
CFLAGS=$(CSTD) -Wall -g

MSYS_USR=C:/msys64/usr
MSYS_BIN=C:/msys64/mingw64/bin

INCLUDE_PATHS=-I. -I$(MSYS_USR)/include
LD_OPENSSL=-L./libs/openssl/lib -lssl -lcrypto -lgdi32
INC_OPENSSL=-I./libs/openssl/include/openssl

LD_GTEST=-lgtest -lgtest_main

LD_BOOSTSERI=-lboost_serialization-mt

INC_WXWID=-IC:/msys64/mingw64/include/wx-3.1 -IC:/msys64/mingw64/lib/wx/include/msw-unicode-3.1
LD_WXWID=-lwx_baseu-3.1 -lwx_mswu_core-3.1 

DIAGNOSTICS=
# DIAGNOSTICS=-DMORE_DIAGNOSTICS

include DLLs

# ----------------------------------------------------------

all: mkbuildir $(BUILD_PATH)/main $(BUILD_PATH)/Tests

update: all cpexe

updateAndShow: update runapp

rebuild: clean mkbuildir $(BUILD_PATH)/main $(BUILD_PATH)/Tests install

.PHONY: runapp
runapp:
	$(INSTALL_PATH)/main.exe &

.PHONY: mkbuilddir
mkbuildir:
ifneq ($(BUILD_PATH), $(BUILD_PATH_EXISTS))
	mkdir -p $(BUILD_DIR)
endif

.PHONY: cpexe
cpexe:
	cp $(BUILD_PATH)/*.exe $(INSTALL_PATH)/

.PHONY: install
install:
ifneq ($(INSTALL_PATH), $(INSTALL_PATH_EXISTS))
	mkdir -p $(INSTALL_DIR)
endif
	cp $(BUILD_PATH)/*.exe $(INSTALL_PATH)/
	for dllfile in $(DLLS) ; do \
    	cp $$dllfile $(INSTALL_PATH)/; \
	done

$(BUILD_PATH)/main: $(BUILD_PATH)/main.o $(BUILD_PATH)/User.o $(BUILD_PATH)/UserArchiver.o $(BUILD_PATH)/Encoder.o $(BUILD_PATH)/Hasher.o $(BUILD_PATH)/Combinations.o $(BUILD_PATH)/WidgetApp.o $(BUILD_PATH)/WidgetFrame.o
	$(CC) $^ $(LD_OPENSSL) $(LD_BOOSTSERI) $(LD_WXWID) -o $@ 

$(BUILD_PATH)/main.o: main.cpp User.h UserArchiver.h Hasher.h Encoder.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS)$(INC_WXWID) $< -o $@

$(BUILD_PATH)/User.o: User.cpp User.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $< -o $@

$(BUILD_PATH)/UserArchiver.o: UserArchiver.cpp UserArchiver.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $< -o $@

$(BUILD_PATH)/Encoder.o: Encoder.cpp Encoder.h Utils.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $< -o $@

$(BUILD_PATH)/Hasher.o: Hasher.cpp Hasher.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $(INC_OPENSSL) $< -o $@

$(BUILD_PATH)/Combinations.o: Combinations.cpp Combinations.h Utils.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $< -o $@

$(BUILD_PATH)/WidgetApp.o: WidgetApp.cpp WidgetApp.h WidgetFrame.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $(INC_WXWID) $< -o $@

$(BUILD_PATH)/WidgetFrame.o: WidgetFrame.cpp WidgetFrame.h
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) $(INC_WXWID) $< -o $@

# Tests
$(BUILD_PATH)/Tests: $(BUILD_PATH)/UserArchiverTests.o $(BUILD_PATH)/UserArchiver.o $(BUILD_PATH)/User.o $(BUILD_PATH)/Hasher.o $(BUILD_PATH)/Encoder.o $(BUILD_PATH)/EncoderTests.o $(BUILD_PATH)/CombinationsTests.o $(BUILD_PATH)/Combinations.o $(BUILD_PATH)/UserTests.o
	$(CC) $^ $(LD_GTEST) $(LD_OPENSSL) $(LD_BOOSTSERI) -o $@

$(BUILD_PATH)/UserArchiverTests.o: UserArchiverTests.cpp
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) -DTEST_DIR=$(ROOTDIR)/$(BUILD_DIR) $< -o $@

$(BUILD_PATH)/EncoderTests.o: EncoderTests.cpp
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) -DTEST_DIR=$(ROOTDIR)/$(BUILD_DIR) $< -o $@

$(BUILD_PATH)/CombinationsTests.o: CombinationsTests.cpp
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) -DTEST_DIR=$(ROOTDIR)/$(BUILD_DIR) $< -o $@

$(BUILD_PATH)/UserTests.o: UserTests.cpp
	$(CC) -c $(CFLAGS) $(DIAGNOSTICS) -DTEST_DIR=$(ROOTDIR)/$(BUILD_DIR) $< -o $@


.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) $(INSTALL_DIR)