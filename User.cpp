#include "User.h"
#include <array>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include "Combinations.h"

User::User() : m_username(m_username_default), m_password(m_password_default)
{
}

User::User(std::string username, std::string password) : m_username(username), m_password(password)
{
}


std::string User::getUsername() const
{
    return m_username;
}


std::string User::getPassword() const
{
    return m_password;
}


User& User::operator=(const User& user) 
{
    m_username = user.m_username;
    m_password = user.m_password;
    prevCombiIdx = user.prevCombiIdx;
    return *this;
}


bool User::operator==(const User& user) const 
{
    std::array<bool, 3> conditions;

    conditions[0] = (m_username == user.m_username);
    conditions[1] = (m_password == user.m_password);
    conditions[2] = (prevCombiIdx == user.prevCombiIdx);

    return std::all_of(conditions.begin(), conditions.end(), [](bool b){return b;});
}


void User::addCombiIdx(int i) 
{
    prevCombiIdx.push_back(i);
}


void User::printCombiIdx() 
{
    for (auto v : prevCombiIdx)
        std::cout << v << "\n";
}


void User::prevCombiIdxToStr() 
{
    prevCombiIdxAsStr = "";
    
    std::stringstream ss;
    for (auto v : prevCombiIdx){
        ss << v << strsep;
    }
    prevCombiIdxAsStr = ss.str();

    // prevCombiIdx.clear();
}


std::string User::getprevCombiIdxAsStr() 
{
    return prevCombiIdxAsStr;
}



void User::combiStrToVec() 
{
    size_t pos = 0;
    std::string token;

    size_t size = strsep.length();

    prevCombiIdx.clear();

    while ((pos = prevCombiIdxAsStr.find(strsep)) != std::string::npos) {
        token = prevCombiIdxAsStr.substr(0, pos);
        prevCombiIdxAsStr.erase(0, pos + size);
        prevCombiIdx.push_back(std::stoi(token));
    }
}


std::vector<std::string> User::getprevCombi() 
{
    auto pCombi = Combinations::getInstance();
    auto prevCombiIdx = getprevCombiIdx();
    std::vector<std::string> prevCombi;
    for (auto it = prevCombiIdx.begin(); it != prevCombiIdx.end()-1; it++){
        prevCombi.push_back(pCombi->getPin(*it));
    }
    return prevCombi;
}


std::vector<int> User::getprevCombiIdx() 
{
    return prevCombiIdx;
}

bool User::addNextCombiIdx() 
{    
    auto pCombinations = Combinations::getInstance();

    // reset if full
    if (prevCombiIdx.size() == pCombinations->getcombiIdxSize())
        prevCombiIdx.clear();

    auto nextIdx = pCombinations->getNextCombiIdx(prevCombiIdx);

    prevCombiIdx.push_back(nextIdx);

    return true;
}

#define INVALIDIDX -1

int User::getCurrentIdx() 
{
    if (prevCombiIdx.empty())
        return INVALIDIDX;
    return prevCombiIdx.back();
}

std::string User::getCurrentPin() 
{
    auto idx = getCurrentIdx();
    if (idx == INVALIDIDX){
        addNextCombiIdx();
        idx = getCurrentIdx();
    }
    auto pCombi = Combinations::getInstance();
    return pCombi->getPin(idx);
}


std::string User::getCurrentPinAndUpdateIdx() 
{
    auto pin = getCurrentPin();
    addNextCombiIdx();
    return pin;
}
