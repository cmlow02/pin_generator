#ifndef __USER_H__
#define __USER_H__

#include <string>
#include <boost/serialization/access.hpp>
#include <vector>


class UserFriend;

class User{

    private:
        friend class UserFriend;
        friend class boost::serialization::access;

        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & m_username;
            ar & m_password;
            ar & prevCombiIdxAsStr;
        }

    public:
        User();
        User(std::string username, std::string password);
        User& operator=(const User& user);
        bool operator==(const User& user) const;

        std::string getUsername() const;
        std::string getPassword() const;
        
        void addCombiIdx(int i);
        void printCombiIdx();
        void prevCombiIdxToStr();
        void combiStrToVec();

        std::vector<int> getprevCombiIdx();
        std::vector<std::string> getprevCombi();
        std::string getprevCombiIdxAsStr();

        int getCurrentIdx();
        bool addNextCombiIdx();
        std::string getCurrentPin();
        std::string getCurrentPinAndUpdateIdx();

    private:
        std::string m_username;
        std::string m_password;
        std::vector<int> prevCombiIdx;
        std::string prevCombiIdxAsStr;
        const std::string strsep = ";";

    public:
        inline static const std::string m_username_default = "username";
        inline static const std::string m_password_default = "password";
        
};

#endif // __USER_H__