#include "UserArchiver.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <filesystem>
#include <iostream>
#include <sstream>
#include <fstream>
#include "Utils.h"

using oarchive = boost::archive::text_oarchive;
using iarchive = boost::archive::text_iarchive;

UserArchiver::UserArchiver() : UserArchiver(nullptr, nullptr)
{
}


UserArchiver::UserArchiver(IEncoder* encoder, IHasher* hasher) : m_encoder(encoder), m_hasher(hasher)
{
}

UserArchiver::~UserArchiver() 
{
    if (!m_encoder)
        delete m_encoder;

    if (!m_hasher)
        delete m_hasher;
}

std::string UserArchiver::getArchiveFileName(std::string s) 
{
    if (m_hasher != nullptr){
        return m_hasher->getHashKey(s) + fileext;
    }
    return s + fileext;
}


bool UserArchiver::encoderPresent() const
{
    return (m_encoder) ? true : false;
}


bool UserArchiver::saveUser(User& user, std::filesystem::path fileloc) 
{
    if (!encoderPresent()) {
        try{
            auto archivename = getArchiveFileName(user.getUsername());

            auto archive_path = fileloc / archivename;
            std::ofstream output_stream(archive_path);
            oarchive output_archive(output_stream);
            output_archive << user;
            return true;
        }
        catch(...){
            throw;
        }
    }
    else{
        auto archivename = getArchiveFileName(user.getUsername());

        user.prevCombiIdxToStr(); // do this before serialization

        std::ostringstream oss;
        oarchive user_serialized(oss);
        user_serialized << user;

        std::string user_serialized_text = oss.str();

        std::string key = user.getPassword();

        #ifdef MORE_DIAGNOSTICS
        std::cout << "\n\n\nuser_serialized_text: " << user_serialized_text << "\n";
        std::cout << "key: " << key << "\n";
        #endif

        unsigned char cipher[max_encoding_len]; //empty

        auto cipher_len = m_encoder->encrypt(key, user_serialized_text, cipher, max_encoding_len);

        #ifdef MORE_DIAGNOSTICS
        // Show cipher
        // ----------------------------------------------------------
        std::cout << "------------------------\nShow cipher\n";
        for (int i = 0; i < cipher_len; i++){
            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)cipher[i];
        }
        std::cout << "\ncipher_len: " << cipher_len;
        std::cout << "\n------------------------\n";
        // ----------------------------------------------------------
        #endif

        // define oarchive
        auto archive_path = fileloc / archivename;
        std::ofstream output_stream(archive_path);
        oarchive output_archive(output_stream);

        std::stringstream ss;
        for (int i=0; i<cipher_len; i++){
            // ss << std::hex << std::setw(2) << std::setfill('0') << (int)cipher[i];
            ss << cipher[i];
        }
    
        auto cipher_str = ss.str();

        #ifdef MORE_DIAGNOSTICS
        std::cout << "cipher_str: " << cipher_str << "\n";
        std::cout << "cipher.size(): "<< cipher_str.size() << "\n";
        std::cout << "cipher_len: " << cipher_len << "\n\n";
        #endif

        output_archive << cipher_str;

        return true;
    }

    return false;
}


UserArchiver::loadStat UserArchiver::loadUser(User& user, std::filesystem::path fileloc) 
{
    auto found = findUser(user, fileloc);

    if (!found)
        return loadStat::NOTFOUND;

    std::ifstream input_stream(fileloc.string());

    iarchive input_archive(input_stream);

    std::string cipher_str;
    input_archive >> cipher_str;

    #ifdef MORE_DIAGNOSTICS
    std::cout << "cipher_str:" << cipher_str << "\n";
    std::cout << "cipher.size(): "<< cipher_str.size() << "\n";
    std::cout << "\n";
    #endif

    std::stringstream ss;
    for (size_t i=0; i<cipher_str.size(); i++){
        if ((i % 2) == 0)
            ss << cipher_str[i];
    }

    #ifdef MORE_DIAGNOSTICS
    std::cout << ss.str() << "\n";
    std::cout << "ss.str().size(): " << ss.str().size() << "\n";
    std::cout << "\n";
    #endif

    unsigned char cipher[max_encoding_len];
    for (auto p = cipher_str.begin(); p != cipher_str.end(); p++){
        cipher[std::distance(cipher_str.begin(), p)] = *p;
    }
    auto cipher_len = cipher_str.size();

    #ifdef MORE_DIAGNOSTICS
    // Show cipher
    // ----------------------------------------------------------
    std::cout << "------------------------\nShow cipher reconstructed\n";
    for (int i = 0; i < cipher_len; i++){
        std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)cipher[i];
    }
    std::cout << "\ncipher_len: " << cipher_len;
    std::cout << "\n------------------------\n";
    // ----------------------------------------------------------
    #endif

    std::string user_serialized_text;

    #ifdef MORE_DIAGNOSTICS
    std::cout << "key: " << user.getPassword() << "\n";
    #endif

    try{
        m_encoder->decrypt(cipher, cipher_len, user.getPassword(), user_serialized_text, max_encoding_len);
    }
    catch(...){
        return loadStat::FOUNDCANTLOAD;
    }

    #ifdef MORE_DIAGNOSTICS
    std::cout << "user_serialized_text: " << user_serialized_text << "\n";
    #endif

    std::stringstream reloaded_stream;
    reloaded_stream.str(user_serialized_text);

    User reloaded_user;

    try{
        iarchive reloaded_archive(reloaded_stream);
        reloaded_archive >> reloaded_user;
    }
    catch(...){
        return loadStat::FOUNDCANTLOAD;
    }

    reloaded_user.combiStrToVec(); // do this after deserialization

    user = reloaded_user;

    return loadStat::FOUNDANDLOADED;
}


bool UserArchiver::findUser(User& user, std::filesystem::path& fileloc)
{
    auto filename = getArchiveFileName(user.getUsername());
    auto archive_path = fileloc / filename;

    for (const auto & entry : std::filesystem::directory_iterator(fileloc)){
        if (entry.path().filename() == filename){
            fileloc = archive_path;
            return true;
        }
    }

    return false;
}

