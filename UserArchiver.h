#ifndef __USERARCHIVER_H__
#define __USERARCHIVER_H__

#include "User.h"
#include <string>
#include <filesystem>
#include "Encoder.h"
#include "Hasher.h"

class UserArchiver
{
    public:
        enum class loadStat{FOUNDANDLOADED, FOUNDCANTLOAD, NOTFOUND};

        UserArchiver();
        UserArchiver(IEncoder* cipher, IHasher* m_hasher);

        bool saveUser(User& user, std::filesystem::path fileloc);
        
        bool findUser(User& user, std::filesystem::path& fileloc);

        loadStat loadUser(User& user, std::filesystem::path fileloc);

        std::string getArchiveFileName(std::string s);

        ~UserArchiver();

    protected:

        bool encoderPresent() const;

    protected:
        IEncoder* m_encoder = nullptr;
        IHasher* m_hasher = nullptr;
        inline static const std::string fileext{".user"};

    private:
        const int max_encoding_len = 4096;
};

#endif // __USERARCHIVER_H__