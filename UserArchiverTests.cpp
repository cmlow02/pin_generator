#include "UserArchiver.h"
#include "User.h"
#include <gtest/gtest.h>
#include <memory>
#include <cstdio>
#include "Utils.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ ":" TOSTRING(__LINE__)

class UserArchiverTests : public ::testing::Test, public UserArchiver
{
    public:
        void SetUp() override{
        }
        void TearDown() override{
        }
};

TEST_F(UserArchiverTests, GetArchiveFileName_NoHasher){
    std::string archivename = getArchiveFileName(User::m_username_default);
    EXPECT_EQ(archivename, User::m_username_default + fileext);
}

TEST_F(UserArchiverTests, getArchiveFileName_WithHasher){
    m_hasher = new OpenSSLHasher;
    std::string archivename = getArchiveFileName(User::m_username_default);

    const int sha256hexlength = 64;
    
    EXPECT_TRUE(archivename.size() == sha256hexlength + fileext.size());
}


TEST_F(UserArchiverTests, SaveUser_NoEncryption_NoHasher){
    User user;

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    ASSERT_NO_THROW(static_cast<void>(saveUser(user, p)));
    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.m_username_default))).string().c_str());
}

TEST_F(UserArchiverTests, SaveUser_NoEncryption_WithHasher){
    m_hasher = new OpenSSLHasher;

    User user;

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    ASSERT_NO_THROW(static_cast<void>(saveUser(user, p)));
    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.m_username_default))).string().c_str());
}

TEST_F(UserArchiverTests, SaveUser_WithEncryption_NoHasher){
    
    m_encoder = new OpenSSLAES;

    User user;

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.m_username_default))).string().c_str());
}

TEST_F(UserArchiverTests, SaveUser_WithEncryption_WithHasher){
    m_hasher = new OpenSSLHasher;
    m_encoder = new OpenSSLAES;

    User user;

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.m_username_default))).string().c_str());
}

TEST_F(UserArchiverTests, FindUser_HashedName){

    // Prepare the file
    m_hasher = new OpenSSLHasher;
    m_encoder = new OpenSSLAES;

    User user;

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    // Try to find
    auto found = findUser(user, p);
    EXPECT_TRUE(found);

    // std::cout << (p / std::filesystem::path(getArchiveFileName(user.getUsername())));

    if (saved)
        std::remove(p.string().c_str());
}

TEST_F(UserArchiverTests, LoadUser_HashedName){

    // Prepare the file
    m_hasher = new OpenSSLHasher;
    m_encoder = new OpenSSLAES;

    User user("User1","MyRandPassword");

    user.addCombiIdx(1);
    user.addCombiIdx(2);
    user.addCombiIdx(3);

    User user_reloaded("User1","MyRandPassword");

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    // Try to load
    loadUser(user_reloaded, p);

    EXPECT_TRUE(user_reloaded == user);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.getUsername()))).string().c_str());
}

TEST_F(UserArchiverTests, UserNotFound){

    User user("User1","MyRandPassword");

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto foundUser = loadUser(user, p);

    EXPECT_EQ(foundUser, UserArchiver::loadStat::NOTFOUND);
}

TEST_F(UserArchiverTests, PasswordIncorrect){
    // Prepare the file
    m_hasher = new OpenSSLHasher;
    m_encoder = new OpenSSLAES;

    User user("User1","MyRandPassword");

    user.addCombiIdx(1);
    user.addCombiIdx(2);
    user.addCombiIdx(3);

    User user_reloaded("User1","SomePassword");

    #ifndef TEST_DIR
    ASSERT_TRUE(false) << "TEST_DIR not set, pass it in as a macro, -DTEST_DIR=, in Makefile";
    #endif

    std::filesystem::path p(std::string(TOSTRING(TEST_DIR)));

    auto saved = saveUser(user, p);
    EXPECT_TRUE(saved);

    // Try to load
    auto loadStat = loadUser(user_reloaded, p);

    EXPECT_EQ(loadStat, UserArchiver::loadStat::FOUNDCANTLOAD);

    if (saved)
        std::remove((p / std::filesystem::path(getArchiveFileName(user.getUsername()))).string().c_str());
}