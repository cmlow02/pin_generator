#include "User.h"
#include <gtest/gtest.h>
#include "Combinations.h"


struct UserTestsF : public ::testing::Test
{
    User user;
    std::vector<int> addIndices{123,456,789};
    std::vector<std::string> expectedPins{"0124", "0457", "0790"};

    void SetUp(){
        user = User("User1","pw");
        for (auto v : addIndices)
            user.addCombiIdx(v);
    }
};

TEST_F(UserTestsF, prevCombiIdxToStr){
    user.prevCombiIdxToStr();

    auto combi = user.getprevCombiIdxAsStr();

    std::string expected = "123;456;789;";

    EXPECT_EQ(combi, expected);
}

TEST_F(UserTestsF, combiStrToVec){

    user.prevCombiIdxToStr();

    user.combiStrToVec();

    auto vec = user.getprevCombiIdx();

    EXPECT_EQ(vec, addIndices);
}

TEST_F(UserTestsF, getCurrentPin){

    auto pin = user.getCurrentPin();

    EXPECT_EQ(pin, expectedPins.back());
    
}

TEST(UserTestEmpty, getCurrentPinNoPreviousIdxLogged){

    User user;
    EXPECT_NO_THROW(user.getCurrentPin());

}

TEST(UserTestEmpty, getCurrentPinAndUpdateIdx){

    User user;
    auto pin1 = user.getCurrentPinAndUpdateIdx();
    auto pin2 = user.getCurrentPinAndUpdateIdx();
    EXPECT_TRUE(pin1.size() > 0);
    EXPECT_TRUE(pin2.size() > 0);
    EXPECT_NE(pin1, pin2);
}


TEST_F(UserTestsF, addNextCombiIdx){

    auto existingidxsize = user.getprevCombiIdx().size();

    user.addNextCombiIdx();

    auto newidxsize = user.getprevCombiIdx().size();
    
    EXPECT_EQ(existingidxsize + 1, newidxsize);

}

class UserFriend : public UserTestsF
{
    public:
    void fillcombiidx(User& user, Combinations* combi){
        user.prevCombiIdx = combi->combiIdx;
    }
};

TEST_F(UserFriend, combiidxfull){

    auto p = Combinations::getInstance();
    fillcombiidx(user, p);

    user.addNextCombiIdx();

    // expect == 1 because it was full, so it was then emptied and repopulated with one value
    auto newidxsize = user.getprevCombiIdx().size();

    EXPECT_EQ(newidxsize, 1);

}