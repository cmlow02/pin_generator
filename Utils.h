#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include <vector>
#include <array>
#include <sstream>

inline void unsigned_char_to_string(unsigned char* char_array, int char_len, std::string& targ_str){
    std::string ss(char_array, char_array + char_len);
    targ_str = std::move(ss);
}


template<class T>
inline constexpr T pow(const T base, unsigned const exponent)
{
    return (exponent == 0) ? 1 : (base * pow(base, exponent-1));
}

constexpr int ncombinations = pow(10,4);

template<typename T>
void swap_rawptr(T *r, T *s)
{
   int temp = *r;
   *r = *s;
   *s = temp;
   return;
} 

#endif // __UTILS_H__