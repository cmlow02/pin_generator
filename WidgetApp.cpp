#include "WidgetApp.h"
#include "WidgetFrame.h"

bool WidgetApp::OnInit()
{
    WidgetFrame *frame = new WidgetFrame();
    frame->Show(true);
    return true;
}