#ifndef __WXAPP_H__
#define __WXAPP_H__

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif


class WidgetApp : public wxApp
{
public:
    virtual bool OnInit();
};



#endif // __WXAPP_H__