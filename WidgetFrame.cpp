#include "WidgetFrame.h"
#include <string>
#include <stdlib.h>
#include <fstream>

auto wxColourGrey = wxColour(128,128,128);
auto wxColourWhite = wxColour(255,255,255);

WidgetFrame::WidgetFrame() : wxFrame(NULL, wxID_ANY, "Pin Generator")
{
    wxMenu *menuFile = new wxMenu;
    menuFile->Append(wxID_EXIT);

    wxMenu *menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);
    
    wxMenuBar *menuBar = new wxMenuBar;
    
    menuBar->Append(menuFile, "&File");
    menuBar->Append(menuHelp, "&Help");
    SetMenuBar( menuBar );
    
    CreateStatusBar();
    SetStatusText("Input User name and password");
    
    Bind(wxEVT_MENU, &WidgetFrame::OnAbout, this, wxID_ABOUT);
    Bind(wxEVT_MENU, &WidgetFrame::OnExit, this, wxID_EXIT);
    
    auto height = 500;
    auto width = 440;

    auto minDim = wxSize(width,height);
    SetMinSize(minDim);
    auto maxDim = wxSize(width,height);
    SetMaxSize(maxDim);

    // ---------------------------------

    wxPanel * main_panel = new wxPanel(this);
    wxBoxSizer * frame_sizer = new wxBoxSizer(wxVERTICAL);
    frame_sizer->Add(main_panel, 0, wxEXPAND);


    const long ID_pinspathLabel = wxNewId(); int ycoord_pinspathLabel = 16;
    const long ID_pinspathTexbox = wxNewId(); int ycoord_pinspathbox = ycoord_pinspathLabel + 20;

    const long ID_usernameLabel = wxNewId(); int ycoord_usernameLabel = ycoord_pinspathbox + 50;
    const long ID_usernameTexbox = wxNewId(); int ycoord_usernameTexbox = ycoord_usernameLabel + 20;

    const long ID_passwordLabel = wxNewId(); int ycoord_passwordLabel = ycoord_usernameTexbox + 50;
    const long ID_passwordTexbox = wxNewId(); int ycoord_passwordTexbox = ycoord_passwordLabel + 20;

    const long ID_loadUserbutton = wxNewId(); int ycoord_loadUserbutton = ycoord_passwordTexbox + 40;
    
    const long ID_pinLabel = wxNewId(); int ycoord_pinLabel = ycoord_loadUserbutton + 50;
    const long ID_pinTexbox = wxNewId(); int ycoord_pinTexbox = ycoord_pinLabel + 20;

    const long ID_genpinbutton = wxNewId(); int ycoord_genpinbutton = ycoord_pinTexbox + 40;

    const long ID_writepinbutton = wxNewId(); int ycoord_writepinbutton = ycoord_genpinbutton + 40;

    auto textBoxSize = wxSize(390,30);

    int leftAlignXcoord = 16;
    
    pinspathLabel = new wxStaticText(main_panel, ID_pinspathLabel, _("Search Folder"), wxPoint(leftAlignXcoord,ycoord_pinspathLabel), wxDefaultSize, 0, _T("ID_pinspathLabel"));
    pinspathTexbox = new wxTextCtrl(main_panel, ID_pinspathTexbox, wxEmptyString, wxPoint(leftAlignXcoord,ycoord_pinspathbox), textBoxSize, 0, wxDefaultValidator, _T("ID_pinspathTexbox"));

    usernameLabel = new wxStaticText(main_panel, ID_usernameLabel, _("Username"), wxPoint(leftAlignXcoord,ycoord_usernameLabel), wxDefaultSize, 0, _T("ID_usernameLabel"));
    usernameTexbox = new wxTextCtrl(main_panel, ID_usernameTexbox, wxEmptyString, wxPoint(leftAlignXcoord,ycoord_usernameTexbox), textBoxSize, 0, wxDefaultValidator, _T("ID_usernameTexbox"));

    passwordLabel = new wxStaticText(main_panel, ID_passwordLabel, _("Password"), wxPoint(leftAlignXcoord,ycoord_passwordLabel), wxDefaultSize, 0, _T("ID_passwordLabel"));
    passwordTexbox = new wxTextCtrl(main_panel, ID_passwordTexbox, wxEmptyString, wxPoint(leftAlignXcoord,ycoord_passwordTexbox), textBoxSize, 0, wxDefaultValidator, _T("ID_passwordTexbox"));
    bool hidepwd = true;
    HWND hwnd = (HWND) passwordTexbox->GetHandle();
    SendMessage(hwnd, EM_SETPASSWORDCHAR, hidepwd ? 0x25cf: 0, 0); // 0x25cf is ● character
    passwordTexbox->Refresh();
    
    // passwordTexbox->SetStyle(wxTE_PASSWORD);
    // passwordTexbox->Refresh();

    loadUserbutton = new wxButton(main_panel, ID_loadUserbutton, _("Load/Create User"), wxPoint(leftAlignXcoord,ycoord_loadUserbutton), textBoxSize, 0, wxDefaultValidator, _T("ID_loadUserbutton"));
    loadUserbutton->Bind(wxEVT_BUTTON, &WidgetFrame::OnloadUserButtonClicked, this);

    genpinbutton = new wxButton(main_panel, ID_genpinbutton, _("Generate Pin"), wxPoint(leftAlignXcoord,ycoord_genpinbutton), textBoxSize, 0, wxDefaultValidator, _T("ID_genpinbutton"));
    genpinbutton->Bind(wxEVT_BUTTON, &WidgetFrame::OngenpinButtonClicked, this);

    pinLabel = new wxStaticText(main_panel, ID_pinLabel, _("Pin Number"), wxPoint(leftAlignXcoord,ycoord_pinLabel), wxDefaultSize, 0, _T("ID_pinLabel"));
    pinTexbox = new wxTextCtrl(main_panel, ID_pinTexbox, wxEmptyString, wxPoint(leftAlignXcoord,ycoord_pinTexbox), textBoxSize, 0, wxDefaultValidator, _T("ID_pinTexbox"));
    pinTexbox->SetEditable(false);

    writePinsToFileButton = new wxButton(main_panel, ID_writepinbutton, _("Write Previous Pins to File"), wxPoint(leftAlignXcoord,ycoord_writepinbutton), textBoxSize, 0, wxDefaultValidator, _T("ID_writepinbutton"));
    writePinsToFileButton->Bind(wxEVT_BUTTON, &WidgetFrame::OnWritePinsButtonClicked, this);

    initApp();
    
}


void WidgetFrame::initApp() 
{
    if (!m_userarchiver)
        initArchiver();
    pinTextBoxOnOff(false);
}

bool WidgetFrame::checkSaveDir() 
{
    savedir = std::string(pinspathTexbox->GetValue().mb_str(wxConvUTF8));

    auto direxists = std::filesystem::exists(savedir);
    
    try{
        if (!direxists){
            std::filesystem::create_directories(savedir);
            wxMessageBox(("Creating new folder in " + savedir.string()).c_str());
        }
    }
    catch(...){
        wxMessageBox(("Unable to create directory: " + savedir.string()).c_str());
        return false;
    }
    
    return true;
}


void WidgetFrame::OnWritePinsButtonClicked(wxCommandEvent& event) 
{
    auto savedirexists = checkSaveDir();
    if ((savedirexists) && (m_user)) {
        auto savedir = std::string(pinspathTexbox->GetValue().mb_str(wxConvUTF8));
        auto pinfile = std::filesystem::path(savedir) / (m_userarchiver->getArchiveFileName(m_user->getUsername()) + ".pins");

        // write to file
        std::ofstream FILE;
        FILE.open(pinfile.c_str());
        auto combi = m_user->getprevCombi();
        for (auto v : combi){
            FILE << v << "\n";
        }
        FILE.close();
    }

}


void WidgetFrame::OnloadUserButtonClicked(wxCommandEvent& event) 
{
    auto savedirexists = checkSaveDir();
    
    if (savedirexists){
        SetStatusText("Pin directory found");
        auto userloaded = loadPinUser();

        if (m_user != nullptr){
            pinTextBoxOnOff(true);
        }
        else{
            pinTextBoxOnOff(false);
        }

    }
}


bool WidgetFrame::initArchiver() 
{
    m_hasher = new OpenSSLHasher;
    m_encoder = new OpenSSLAES;
    m_userarchiver = new UserArchiver(m_encoder, m_hasher);

    if ((m_hasher) && (m_encoder) && (m_userarchiver))
        return true;

    return false;
}


void WidgetFrame::resetUser() 
{
    delete m_user;
    m_user = nullptr;
}


void WidgetFrame::pinTextBoxOnOff(bool On) 
{
    if (!On){
        pinTexbox->SetBackgroundColour(wxColourGrey);
        pinTexbox->SetStyle(0, -1, pinTexbox->GetDefaultStyle());
        pinTexbox->Refresh();
    }
    else{
        pinTexbox->SetBackgroundColour(wxColourWhite);
        pinTexbox->SetStyle(0, -1, pinTexbox->GetDefaultStyle());
        pinTexbox->Refresh();
    }
}


bool WidgetFrame::findAndLoad() 
{
    auto loadStat = m_userarchiver->loadUser(*m_user, savedir);

    bool returnStat = false;

    switch (loadStat)
    {
    case UserArchiver::loadStat::NOTFOUND:
        {
            SetStatusText("User not found.");
            wxMessageBox("No records found. Creating new user.");
            auto saved = m_userarchiver->saveUser(*m_user, savedir);
            if (saved)
                SetStatusText("Created new user.");
            returnStat = true;
        }
        break;
    case UserArchiver::loadStat::FOUNDCANTLOAD:
        {
            wxMessageBox("Found user but unable to load. Possibly due to a wrong password.");
            SetStatusText("Unable to load user.");
            passwordTexbox->Clear();
            resetUser();
            returnStat = false;
        }
        break;
    case UserArchiver::loadStat::FOUNDANDLOADED:
        {
            SetStatusText("User loaded.");
            returnStat = true;
        }
    default:
        break;
    }

    return returnStat;
}

#define MINPASSWORDLEN 14

bool WidgetFrame::loadPinUser() 
{
    auto inputUsername = std::string(usernameTexbox->GetValue().mb_str(wxConvUTF8));
    auto inputPassword = std::string(passwordTexbox->GetValue().mb_str(wxConvUTF8));

    if (inputPassword.size() < MINPASSWORDLEN){
        wxMessageBox("Passwords need to be at least 14 characters long.");
        return false;
    }

    bool loaded = false;

    if (!m_user){
        m_user = new User(inputUsername, inputPassword);
        loaded = findAndLoad();
    }
    else {
        if (m_user->getUsername() != inputUsername){
            savedata();
            delete m_user; 
            m_user = new User(inputUsername, inputPassword);
            loaded = findAndLoad();
            pinTexbox->Clear();
            return true;
        }
        else{
            SetStatusText("User " + m_user->getUsername() + " is loaded.");
            return true;
        }
    }
}


void WidgetFrame::savedata()
{
    checkSaveDir();
    m_userarchiver->saveUser(*m_user, savedir);
}


void WidgetFrame::OngenpinButtonClicked(wxCommandEvent& event)
{
    std::string pin;
    if (m_user)
        pinTexbox->SetValue(m_user->getCurrentPinAndUpdateIdx());

    savedata();
}

void WidgetFrame::OnExit(wxCommandEvent& event)
{
    m_userarchiver->saveUser(*m_user, savedir);
    Close(true);
}

void WidgetFrame::execAbout(){
    std::string about{"Step 1: Specify the folder where the pins are stored\nStep 2: Input User name and password\nStep 3: Click on Load/Create User\nStep 4: Click on Generate Pin"};
    std::string howto{"HowTo"};

    wxMessageBox(about,
                 howto, wxOK | wxICON_INFORMATION);
}

void WidgetFrame::OnAbout(wxCommandEvent& event)
{
    execAbout();
}
