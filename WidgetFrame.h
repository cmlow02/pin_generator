#ifndef __WXFRAME_H__
#define __WXFRAME_H__

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/textdlg.h>

#include "User.h"
#include "UserArchiver.h"
#include "Hasher.h"
#include "Encoder.h"
#include <filesystem>

class WidgetFrame : public wxFrame
{
public:
    WidgetFrame();
private:
    void OnHello(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OngenpinButtonClicked(wxCommandEvent& event);
    void OnloadUserButtonClicked(wxCommandEvent& event);
    void OnWritePinsButtonClicked(wxCommandEvent& event);

    wxStaticText* pinspathLabel;
    wxTextCtrl* pinspathTexbox;

    wxStaticText* usernameLabel;
    wxTextCtrl* usernameTexbox;
    
    wxStaticText* passwordLabel;
    wxTextCtrl* passwordTexbox;

    wxButton* genpinbutton;
    wxButton* loadUserbutton;
    wxButton* writePinsToFileButton;
    
    wxStaticText* pinLabel;
    wxTextCtrl* pinTexbox;

    std::filesystem::path savedir;

    bool checkSaveDir();
    bool loadPinUser();
    bool findAndLoad();
    bool initArchiver();
    void initApp();
    void resetUser();
    void execAbout();
    void savedata();

    void pinTextBoxOnOff(bool On);

    IHasher* m_hasher;
    IEncoder* m_encoder;

    User* m_user = nullptr;
    UserArchiver* m_userarchiver = nullptr;
    
};




#endif // __WXFRAME_H__